package exercise_phase_1;
import java.util.*;

public class Ex_16 {
	public static void main(String[] args) {
		int[] arr = {1, 2, 2, 3, 3, 3, 4, 4, 4, 4, 5, 5};
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the number to be searched in the array :");
		int n = sc.nextInt();
		int count = 0;
		for(int i=0; i<arr.length; i++) {
			if(n == arr[i]) {
				count = count+1;
			}
			else {
				continue;
			}
		}
		if(count>0) {
			System.out.print(n+" is present and is repeated "+count+" times.");
		}
		else {
			System.out.print(n+" is not present.");
		}
	}
}
