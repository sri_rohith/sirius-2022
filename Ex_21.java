package exercise_phase_1;
import java.util.*;

public class Ex_21 {
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter a month :");
		String month = sc.next();
		switch(month.toLowerCase()) {
			case "january":
				System.out.print(31+" days");
				break;
		
			case "february":
				System.out.print("Enter the year :");
				int year = sc.nextInt();
				if(year%4==0) {
					System.out.print(29+" days");
				}
				else {
					System.out.print(28+" days");
				}
				break;
			
			case "march":
				System.out.print(31+" days");
				break;
				
			case "april":
				System.out.print(30+" days");
				break;
				
			case "may":
				System.out.print(31+" days");
				break;
				
			case "june":
				System.out.print(30+" days");
				break;
				
			case "july":
				System.out.print(31+" days");
				break;
				
			case "august":
				System.out.print(31+" days");
				break;
				
			case "september":
				System.out.print(30+" days");
				break;
				
			case "october":
				System.out.print(31+" days");
				break;
				
			case "november":
				System.out.print(30+" days");
				break;
				
			case "december":
				System.out.print(31+" days");
				break;
		}
	}
	
}
