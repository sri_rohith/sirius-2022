package exercise_phase_1;

public class Ex_13 {
	public static void main(String[] args){
		int[][] class_marks = {{50, 55, 60, 65}, {60, 65, 70, 75}, {70, 75, 80, 85}};
		for(int i=0; i < class_marks.length; i++) {
			int count = 0;
			for(int j=0; j < class_marks[i].length; j++) {
				count += class_marks[i][j];
			}
			System.out.println("The result of Student "+(i+1)+" is "+count+".");
			count=0;
		}
	}	
}
