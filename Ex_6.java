package exercise_phase_1;

public class Ex_6 {
	public static void main(String[] args) {
    	int[] numberArray = { 1, 2, 3, 4};
    	System.out.println("Array before Resizing: ");
        for (int i = 0; i < numberArray.length; i++) {
            System.out.print(numberArray[i]+" ");
        }
 
        int[] temp = new int[6];
        int length = numberArray.length;
        for (int j = 0; j < length; j++) {
          temp[j] = numberArray[j];
        }
 
        System.out.println("\nArray after Resizing: ");
        for (int i = 0; i < temp.length; i++) {
            System.out.print(temp[i]+" ");
        }
   }
}
