package exercise_phase_1;

public class Ex_3 {
	public static void main(String[]args) {
		
		byte b = 20;
		int i = 10;
		long l = 20;
		float f = 40;
		double d = 30.01;
		
		int bi = b;
		long il = i;
		float lf = l;
		double fd = f;
		
		float df = (float) d;
		long fl = (long) f;
		int li = (int) l;
		byte ib = (byte)i;
		
		System.out.println("implicit byte->int="+bi+" int->long="+il+" long->float="+lf+" float->double="+fd);
		System.out.println("explicit double->float"+df+" float->long="+fl+" long->int="+li+" int->byte="+ib);
		
	}
}
