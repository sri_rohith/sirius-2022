package exercise_phase_1;

public class Ex_17 {
	public static final int days_in_a_week = 7;
	public static void main(String[] args) {
		days_in_a_week = 8;     //constant values cannot be overwritten in java. 
		System.out.println(days_in_a_week);
		function_a();			//Cannot make static reference into a non-static method.
	}
	
	void function_a(){
		System.out.println(days_in_a_week);
	}
}
