package exercise_phase_1;

public class Ex_9 {
	public static void main(String[] args)
    {
        int [] arr = {10, 20, 30, 40, 50};
        System.out.println("Array:");
        for(int i=0; i<arr.length; i++) {
        	System.out.print(arr[i]+" ");
        }
        int[] b = new int[arr.length];
        int len = arr.length;
        for (int i = 0; i < arr.length; i++) {
            b[len - 1] = arr[i];
            len -= 1;
        }
        System.out.println("\nReversed array: ");
        for (int k = 0; k < arr.length; k++) {
            System.out.print(b[k]+" ");
        }
    }
}
