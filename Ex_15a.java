package exercise_phase_1;

public class Ex_15a {
	public static void main(String[] args) {
		System.out.println("The prime numbers are :");
		for(int i=0; i<=100; i++){
			int j = 0;
			for (int k=1; k<=i; k++) {
				if (i%k == 0) { 
					j++;
				}
			}	
			if (j==2) {
				System.out.print(i+" ");
			}
			else {
				continue;
			}
		}
	}
}
