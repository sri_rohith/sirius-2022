package exercise_phase_1;
import java.util.*;

public class Ex_18 {
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter a number :");
		int num = sc.nextInt();
		String[] arr = {"zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten"};
		int temp, digit, count = 0;
		temp = num;
		while(temp>0) {
			temp /= 10;
			count++;
		}
		String[] result = new String[count];
		temp = num;
		while(temp>0) {
			digit = temp%10;
			result[count-1] = arr[digit];
			count -= 1;
			temp /= 10;
		}
		for(int i=0; i<result.length; i++) {
			System.out.print(result[i]+" ");
		}
	}
	
}
