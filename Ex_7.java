package exercise_phase_1;
import java.util.Arrays;
import java.util.Collections;

public class Ex_7 {
	public static void main(String[] args) {
		
		Integer[] arr = {1, 9, 2, 8, 3, 7, 4, 6, 5};
		System.out.print("Normal order :");
		for (int i=0; i<arr.length; i++){
			System.out.print(arr[i]+" ");
		}
		
		Arrays.sort(arr);
		System.out.println("\nAscending Order :"+Arrays.toString(arr));
		
		Arrays.sort(arr, Collections.reverseOrder());
		System.out.println("Descending Order :"+Arrays.toString(arr));
	}
}
