package exercise_phase_1;

public class Ex_22 {
	public static void main(String[] args) {
		validation c = new validation();
		System.out.println(c._public);
		System.out.println(c._protected);
		System.out.println(c._default);
		System.out.println(c._private);	//the private member throws error.
	}
}

class validation {
	public int _public = 5;
	protected int _protected = 5;
	int _default = 5;
	private int _private = 5;
}
