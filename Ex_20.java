package exercise_phase_1;

public class Ex_20 {
	public static void main(String[] args) {
		int[][] arr = {{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}, {13, 14, 15, 16}};
		for(int i=0; i<arr.length; i++) {
			int large = 0;
			for(int j=0; j<arr[i].length; j++) {
				if(arr[i][j]>large) {
					large = arr[i][j];
				}
				else {
					continue;
				}
			}
			System.out.println("The largest number in array "+(i+1)+" is "+large+".");
		}
	}
}
