package exercise_phase_1;
/**
*
* @author Sri_Rohith
*/
public class Ex_14a {
	public static void main(String[] args) {
		/**
	    * This is the main method.
	    */
		int a= 10;
		int b= 20;
		/**
	    * Declared two variables a and b.
	    */
		int c = a + b;
		/**
		* where c is the addition of a and b.
		*/
		System.out.println(c);
		/**
        * Using standard output stream
        * for giving the output.
	    */
	}
}
