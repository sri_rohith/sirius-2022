package exercise_phase_1;

public class Ex_27 {
	public static void main(String[] args) {
		int amnt = 14000;
		double year_1 = amnt+(amnt*0.4);
		double year_2 = year_1-1500;
		double year_3 = year_2+(year_2*0.12);
		System.out.println("At year 1, the amount increases as $"+year_1+", and at year 2, the price decreases to $"+year_2+", and at year 3, the amount increases as $"+year_3);
	}
}
