package exercise_phase_1;
import java.util.*;
import java.lang.Math;

public class Ex_15b {
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the number to be checked :");
		int num = sc.nextInt();
		System.out.println(AmstrongNumber(num));
		System.out.println(PerfectNumber(num));
		System.out.println(PalindromeNumber(num));
	}
	
	static String AmstrongNumber(int n) {
		int temp, length=0, last=0, sum=0;
		temp = n;
		while(temp>0) {
			temp /= 10;
			length++;
		}
		temp = n;
		while(temp>0) {
			last = temp%10;
			sum += (Math.pow(last, length));
			temp /= 10;
		}
		if(n==sum) {
			return(n+" is an amstrong number.");
		}
		else return(n+" is not an amstrong number.");
	}
	
	static String PerfectNumber(int n) {
		int sum =0, i =1;
		while(i<=n/2) {
			if(n%i==0) {
				sum += i;
			}
			i++;
		}
		if(sum==n) {
			return(n+" is a perfect number.");
		}
		else return(n+" is not a perfect number.");
	}
	
	static String PalindromeNumber(int n) {
		int reverse = 0, rem;
		int num = n;
		while(n!=0) {
			rem = n%10;
			reverse = reverse*10+rem;
			n /= 10;
		}
		if(num==reverse) {
			return(num+" is a palindrome number.");
		}
		else return(num+" is not a palindrome number.");
	}
	
}
