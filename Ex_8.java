package exercise_phase_1;
import java.util.*;

public class Ex_8 {
	
    public static void main(String[] args){
        int[] my_array = { 5, 4, 6, 1, 3, 2, 7, 8, 9 };
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the element to be searched for index position :");
        int element = sc.nextInt();
        int length = my_array.length;
        int i = 0;
        while (i < length) {
            if (my_array[i] == element) {
                System.out.println("Index position of "+element+" is: "+ i);
                break;
            }
            else {
                i += 1;
                if(i == length) {
                	System.out.print(-1);
                }
            }
        }   
    }
    
}
