package exercise_phase_1;

public class Ex_19 {
	public static void main(String[] args) {
		int[][] arr = {{1, 2, 3, 4},{5, 6, 7, 8},{9, 10, 11, 12},{13, 14, 15, 16}};
		for(int i=0; i<arr.length; i++) {
			for(int j=0; j<arr[i].length; j++) {
				System.out.print(arr[i][j]+" ");
			}
			System.out.print("\n");
		}
		for(int i=0; i < arr.length; i++) {
			int[] b = new int[arr.length];
			int len = arr.length;
			for (int j = 0; j < arr[i].length; j++) {
				b[len - 1] = arr[i][j];
				len -= 1;
			}
			arr[i] = b;
		}
		System.out.print("\n");
		for(int i=0; i<arr.length; i++) {
			for(int j=0; j<arr[i].length; j++) {
				System.out.print(arr[i][j]+" ");
			}
			System.out.print("\n");
		}
	}
}
